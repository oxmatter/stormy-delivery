﻿using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private float followSpeed = 0.75f;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float limitX;
    [SerializeField] private float limitY;
    [SerializeField] private Transform target;

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    private void FixedUpdate()
    {
        if (target == null) return;

        Vector3 trPos = Vector3.Lerp(transform.position, offset + target.position, followSpeed * Time.deltaTime);

        if (Math.Abs(limitX) > 0.1f)
        {
            trPos.x = Mathf.Clamp(trPos.x, -limitX, limitX);
        }
        
        if (Math.Abs(limitY) > 0.1f)
        {
            trPos.y = Mathf.Clamp(trPos.y, -limitY, limitY);
        }

        transform.position = trPos;
    }
}
