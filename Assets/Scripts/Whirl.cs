﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whirl : MonoBehaviour
{
    [SerializeField] private float damageTime = 0.5f;
    [SerializeField] private float damageAmount = 10f;
    private float _timer;

    private void Update()
    {
        _timer -= Time.deltaTime;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (_timer <= 0)
        {
            if (!other.transform.TryGetComponent(out IDamageable damageable)) return;
            damageable.Damage(damageAmount);
            _timer = damageTime;
        }
    }
}
