﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoatMover), typeof(AudioSource))]
public class Player : MonoBehaviour, IDamageable
{
    [SerializeField] private Cannon cannon;
    [SerializeField] private Transform weaponTarget;
    [SerializeField] private float startHealth = 100f;
    [SerializeField] private AudioClip hitSound;
    public static Action OutOfRange = delegate {  };
    private BoatMover _boatMover;
    private Health _health;
    private AudioSource _audioSource;
    private bool _canControl;
    private Transform _transform;

    public Health Health => _health;
    public float VelocityMagnitude { private set; get; }

    private void Awake()
    {
        _boatMover = GetComponent<BoatMover>();
        _audioSource = GetComponent<AudioSource>();
        _health = new Health(startHealth);
        cannon.SetUp(weaponTarget, 25f);
        _health.OnHealthDepleted += () =>
        {
            gameObject.SetActive(false);
        };
        _health.OnTookDamage += (amount) =>
        {
            _audioSource.PlayOneShot(hitSound);
        };
        _transform = transform;
    }

    public void Activate()
    {
        _canControl = true;
    }

    private void Update()
    {
        if (Vector2.Distance(_transform.position, Vector2.zero) > 20) OutOfRange.Invoke(); 
        VelocityMagnitude = _boatMover.Velocity.magnitude;
        if (!_canControl) return;
        _boatMover.Move(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
        if (Input.GetButtonDown("Fire1")) cannon.Fire();
    }

    public void Damage(float amount)
    {
        _health.TakeDamage(amount);
    }
}
