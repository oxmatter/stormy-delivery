﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WavePlacer : MonoBehaviour
{
    [SerializeField] private Transform wavePref;
    [SerializeField] private Vector2 mapSize;
    [SerializeField] private int wavesAmount = 10;

    private void Start()
    {
        for (int i = 0; i < wavesAmount; i++)
        {
            Vector2 rndPos = new Vector2(Random.Range(-mapSize.x, mapSize.x), Random.Range(-mapSize.y, mapSize.y));
            Transform tr = Instantiate(wavePref, rndPos, Quaternion.identity).transform;
            tr.parent = transform;
        }
    }
}
