﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BoatMover : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float turnSpeed;
    private Rigidbody2D _rb;
    private Vector2 _dir;
    public Vector2 Velocity { private set; get; }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector2 dir)
    {
        _dir = dir.normalized;
    }

    private void FixedUpdate()
    {
        // _rb.MovePosition(_rb.position + (Vector2)transform.up * (Time.deltaTime * moveSpeed * _dir.y));
        _rb.AddForce((Vector2)transform.up * (Time.deltaTime * moveSpeed * _dir.y));
        float zEulerAngles = transform.eulerAngles.z;
        zEulerAngles -= _dir.x * turnSpeed * Time.deltaTime;
        _rb.MoveRotation(Quaternion.Euler(0, 0, zEulerAngles));
        Velocity = _rb.velocity;
    }
}
