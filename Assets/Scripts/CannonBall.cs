﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    private bool _isFromEnemy;
    public bool IsFromEnemy => _isFromEnemy;
    private Rigidbody2D _rb;
    private float _timeAlive;
    private float _damageAmount;
    private Transform _ignoreTransform;

    public void SetUp(bool isFromEnemy, float damageAmount, Transform ignoreTransform)
    {
        _isFromEnemy = isFromEnemy;
        _damageAmount = damageAmount;
        _ignoreTransform = ignoreTransform.root;
    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _timeAlive += Time.deltaTime;
        if (_timeAlive >= 5)
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        _rb.velocity = transform.up * 10f;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform == _ignoreTransform || !other.transform.TryGetComponent(out IDamageable damageable)) return;
        damageable.Damage(_damageAmount);
        Destroy(gameObject);
        // if ((damageable is Enemy && !_isFromEnemy) || (damageable is Player && _isFromEnemy))
        // {
        //     damageable.Damage(_damageAmount);
        //     Destroy(gameObject);
        // }
    }
}
