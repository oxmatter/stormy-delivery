﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveAnimator : MonoBehaviour
{
    [SerializeField] private Vector2 timerRange;
    private Animator _animator;
    private float _timerCounter;
    private static readonly int WaveTrigger = Animator.StringToHash("WaveTrigger");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        NextTimer();
    }

    private void Update()
    {
        _timerCounter -= Time.deltaTime;
        if (_timerCounter <= 0)
        {
            MakeAction();
            NextTimer();
        }
    }

    private void MakeAction()
    {
        _animator.SetTrigger(WaveTrigger);
    }

    private void NextTimer()
    {
        _timerCounter = Random.Range(timerRange.x, timerRange.y);
    }
}
