﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(BoatMover), typeof(AudioSource))]
public class Enemy : MonoBehaviour, IDamageable
{
    [SerializeField] private Cannon cannon;
    [SerializeField] private float startHealth = 100f;
    [SerializeField] private AudioClip hitSound;
    private Transform _target;
    private BoatMover _boatMover;
    private float _internalShootTimer;
    private Transform _transform;
    private Health _health;
    private AudioSource _audioSource;
    public Action<Enemy> OnDestroyed = delegate {  };
    
    private void Awake()
    {
        _target = GameObject.FindWithTag("Player").transform;
        _boatMover = GetComponent<BoatMover>();
        _audioSource = GetComponent<AudioSource>();
        _health = new Health(startHealth);
        _transform = transform;
        cannon.SetUp(_target, 10f);
        ResetShootTimer();
        _health.OnHealthDepleted += () =>
        {
            OnDestroyed.Invoke(this);
            Destroy(gameObject);
        };
        _health.OnTookDamage += (amount) =>
        {
            _audioSource.PlayOneShot(hitSound);
        };
    }

    private void Update()
    {
        _internalShootTimer -= Time.deltaTime;
        float targetDistance = Vector2.Distance(_transform.position, _target.position);

        Vector2 targetDir = _target.position - _transform.position;
        float angleToTarget = Vector2.SignedAngle(targetDir, transform.up);
        Vector2 dir;
        if (angleToTarget > 2)
        {
            dir.x = 1;
        } else if (angleToTarget < -2)
        {
            dir.x = -1;
        }
        else
        {
            dir.x = 0;
        }
        dir.y = Mathf.InverseLerp(0, 5, targetDistance);
        _boatMover.Move(dir);

        if (_internalShootTimer <= 0 && targetDistance <= 8)
        {
            cannon.Fire();
            ResetShootTimer();
        }
    }

    private void ResetShootTimer()
    {
        _internalShootTimer = Random.Range(2.0f, 5.0f);
    }

    public void Damage(float amount)
    {
        _health.TakeDamage(amount);
    }
}
