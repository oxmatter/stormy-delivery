﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private EnemySpawner enemySpawner;
    [SerializeField] private Player player;
    [SerializeField] private TaskManager taskManager;
    public int Score { private set; get; }
    public Action<int> ScoreChanged = delegate {  };
    public Action GameStarted = delegate {  };
    private void Awake()
    {
        taskManager.TaskCompleted += () =>
        {
            Score += 1;
            ScoreChanged.Invoke(Score);
        };
        Player.OutOfRange += () =>
        {
            RestartGame();
        };
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartGame()
    {
        enemySpawner.StartSpawning();
        taskManager.StartGeneratingTasks();
        player.Activate();
        GameStarted.Invoke();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
