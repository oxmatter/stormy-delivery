﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Dock : MonoBehaviour
{
    [SerializeField] private GameObject dockTriggerVisual;
    [SerializeField] private float timeToCompleteTask = 5f;
    [SerializeField] private AudioClip taskStartedAudio;
    [SerializeField] private AudioClip taskCompletedAudio;
    public static Action TaskCompleted = delegate {  };
    public static Action<float> TaskProgress = delegate {  };
    private float _timePlayerInArea;
    private bool _isActive;
    private AudioSource _audioSource;
    private bool progressAudioStarted;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Activate()
    {
        _isActive = true;
        dockTriggerVisual.SetActive(true);
    }

    private void Update()
    {
        if (_isActive && _timePlayerInArea > timeToCompleteTask)
        {
            _audioSource.PlayOneShot(taskCompletedAudio);
            TaskCompleted.Invoke();
            _isActive = false;
            dockTriggerVisual.SetActive(false);
            //TODO: Play sound
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.TryGetComponent(out Player pl))
        {
            if (pl.VelocityMagnitude < 0.5f && _isActive)
            {
                if (!progressAudioStarted)
                    _audioSource.PlayOneShot(taskStartedAudio);
                progressAudioStarted = true;
                _timePlayerInArea += Time.deltaTime;
                TaskProgress.Invoke(_timePlayerInArea / timeToCompleteTask);
            }
            else
            {
                _timePlayerInArea = 0;
                progressAudioStarted = false;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Player pl))
        {
            _timePlayerInArea = 0;
            progressAudioStarted = false;
        }
    }
}
