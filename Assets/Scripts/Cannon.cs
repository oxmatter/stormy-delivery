﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Cannon : MonoBehaviour
{
    [SerializeField] private Transform cannonBall;
    [SerializeField] private Transform cannonStart;
    [SerializeField] private bool isEnemy;
    [SerializeField] private AudioClip shootSound;
    private Camera _mainCamera;
    private Transform _target;
    private Transform _transform;
    private float _damageAmount;
    private AudioSource _audioSource;

    public void SetUp(Transform target, float damageAmount)
    {
        _target = target;
        _damageAmount = damageAmount;
    }

    private void Awake()
    {
        _mainCamera = Camera.main;
        _transform = transform;
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        Vector3 targetPos = _target.position;
        Vector3 transformPos = _transform.position;
        targetPos.x -= transformPos.x;
        targetPos.y -= transformPos.y;
        targetPos.z = 0f;
        float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg - 90f;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    public void Fire()
    {
        CannonBall ball = Instantiate(cannonBall, cannonStart.position, cannonStart.rotation)
            .GetComponent<CannonBall>();
        ball.SetUp(isEnemy, _damageAmount, _transform.root);
        _audioSource.PlayOneShot(shootSound);
    }
}
