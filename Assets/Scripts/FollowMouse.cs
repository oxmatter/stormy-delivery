﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    private Transform _transform;
    private Camera _mainCam;

    private void Awake()
    {
        _transform = transform;
        _mainCam = Camera.main;
    }
    
    void Update()
    {
        _transform.position = (Vector2)_mainCam.ScreenToWorldPoint(Input.mousePosition);
    }
}
