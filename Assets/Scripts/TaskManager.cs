﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TaskManager : MonoBehaviour
{
    [SerializeField] private Dock startDock;
    [SerializeField] private Dock[] otherDocks;
    public Action<Dock> DockChanged = delegate {  };
    public Action<float> TaskProgress = delegate {  };
    public Action TaskCompleted;
    private List<Dock> _docks = new List<Dock>();
    private Dock _lastDock;
    private Dock _nextDock;
    private bool _spawnedSome;

    private void Awake()
    {
        _docks.Add(startDock);
        foreach (var dock in otherDocks)
        {
            _docks.Add(dock);
        }
        _lastDock = startDock;
        _nextDock = startDock;
        
        Dock.TaskCompleted += ChangeDock;
        Dock.TaskProgress += TaskProgressChanged;
    }

    private void OnDisable()
    {
        Dock.TaskCompleted -= ChangeDock;
        Dock.TaskProgress -= TaskProgressChanged;
    }

    public void StartGeneratingTasks()
    {
        ChangeDock();
    }

    private void ChangeDock()
    {
        _lastDock = _nextDock;
        if (_spawnedSome)
            TaskCompleted.Invoke();
        Dock newDock = GetNextDock();
        newDock.Activate();
        _nextDock = newDock;
        DockChanged(_nextDock);
        _spawnedSome = true;
    }

    private void TaskProgressChanged(float progress)
    {
        TaskProgress.Invoke(progress);
    }

    private Dock GetNextDock()
    {
        List<Dock> candidates = _docks.Where(it => it != _lastDock).ToList();
        return candidates[Random.Range(0, candidates.Count)];
    }
}
