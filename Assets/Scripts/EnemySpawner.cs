﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Enemy enemyPrefab;
    [SerializeField] private Player player;
    private List<Transform> _spawns = new List<Transform>();
    private List<Enemy> _spawnedEnemies = new List<Enemy>();
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
        foreach (Transform childTransform in _transform)
        {
            _spawns.Add(childTransform);
        }
    }

    public void StartSpawning()
    {
        StartCoroutine(SpawnRoutine());
    }

    IEnumerator SpawnRoutine()
    {
        while (player.Health.CurrentHealth > 0)
        {
            float nextDelay = Random.Range(5f, 10f);
            if (_spawnedEnemies.Count < 10)
            {
                SpawnEnemy();
            }
            yield return new  WaitForSeconds(nextDelay);
        }
    }

    private void SpawnEnemy()
    {
        Transform point = RandomSpawnPont();
        Enemy spEnemy = Instantiate(enemyPrefab, point.position, point.rotation).GetComponent<Enemy>();
        _spawnedEnemies.Add(spEnemy);
        spEnemy.OnDestroyed += (enemy) =>
        {
            _spawnedEnemies.Remove(enemy);
        };
    }

    private Transform RandomSpawnPont()
    {
        return _spawns[Random.Range(0, _spawns.Count)];
    }
}
