﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [SerializeField] private RectTransform targetMarker;
    [SerializeField] private TaskManager taskManager;
    [SerializeField] private Slider taskProgressBar;
    [SerializeField] private Slider playerHpBar;
    [SerializeField] private GameObject taskCompletedLabel;
    [SerializeField] private TextMeshProUGUI scoreLabel;
    [SerializeField] private GameObject inGameUi;
    [SerializeField] private GameObject menuUi;
    [SerializeField] private Player player;
    [SerializeField] private GameManager gameManager;
    private Vector3 _mapTargetLocation = Vector3.zero;
    private Camera _mainCam;
    private bool _taskCompletedShowing;
    private float _taskCompleteTimer;
    
    private void Awake()
    {
        inGameUi.SetActive(false);
        menuUi.SetActive(true);
        _mainCam = Camera.main;
        targetMarker.gameObject.SetActive(false);
        taskManager.DockChanged += dock =>
        {
            targetMarker.gameObject.SetActive(true);
            SetTargetLocation(dock.transform.position);
        };
        taskManager.TaskProgress += progress =>
        {
            taskProgressBar.gameObject.SetActive(true);
            taskProgressBar.value = progress;
        };
        taskManager.TaskCompleted += () =>
        {
            _taskCompleteTimer = 0;
            _taskCompletedShowing = true;
            taskCompletedLabel.SetActive(true);
            taskProgressBar.gameObject.SetActive(false);
        };
        gameManager.ScoreChanged += score =>
        {
            scoreLabel.text = $"Deliveries: {score}";
        };
        gameManager.GameStarted += () =>
        {
            inGameUi.SetActive(true);
            menuUi.SetActive(false);
        };
    }

    private void UpdatePlayerHp(float current, float max)
    {
        playerHpBar.value = current / max;
    }

    private void Update()
    {
        Vector2 markerPos = _mainCam.WorldToScreenPoint(_mapTargetLocation);
        markerPos.x = Mathf.Clamp(markerPos.x, 0, Screen.width);
        markerPos.y = Mathf.Clamp(markerPos.y, 0, Screen.height);
        targetMarker.position = markerPos;
        if (_taskCompletedShowing)
        {
            _taskCompleteTimer += Time.deltaTime;
        }

        if (_taskCompletedShowing && _taskCompleteTimer > 2)
        {
            _taskCompletedShowing = false;
            taskCompletedLabel.SetActive(false);
        }
        UpdatePlayerHp(player.Health.CurrentHealth, player.Health.MaxHealth);
    }

    private void SetTargetLocation(Vector2 pos, bool visible = true)
    {
        _mapTargetLocation = pos;
        targetMarker.gameObject.SetActive(targetMarker);
    }
}
