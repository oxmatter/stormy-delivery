﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health
{
    public Action<float> OnTookDamage = delegate { };
    public Action OnHealthDepleted = delegate { };
    public Action<float> OnHealthRestored = delegate { };

    public float CurrentHealth { get; private set; }

    public float MaxHealth { get; private set; }
    public bool CanTakeDamage;

    public Health(float maxHealth, bool canTakeDamage = true)
    {
        MaxHealth = maxHealth;
        CurrentHealth = maxHealth;
        CanTakeDamage = canTakeDamage;
    }

    public void InstaKill()
    {
        TakeDamage(CurrentHealth * 2);
    }

    public void TakeDamage(float amount)
    {
        if (!CanTakeDamage) return;
        float amountToRemove = Math.Min(CurrentHealth, amount);
        CurrentHealth -= amountToRemove;
        OnTookDamage(amountToRemove);
        if (CurrentHealth <= 0)
        {
            CanTakeDamage = false;
            OnHealthDepleted();
        }
    }

    public void RestoreHealth(float amount)
    {
        float restoreAmount = Math.Min(amount, (MaxHealth - CurrentHealth));
        CurrentHealth += restoreAmount;
        OnHealthRestored(restoreAmount);
    }

    public void ResetHealth()
    {
        CurrentHealth = MaxHealth;
    }

    public void SetMaxHealth(float newMaxHealth)
    {
        MaxHealth = newMaxHealth;
        ResetHealth();
    }

    public void SetHealth(float amount)
    {
        if (amount > MaxHealth) amount = MaxHealth;
        CurrentHealth = amount;
    }

    public bool HasMaxHp()
    {
        return Math.Abs(CurrentHealth - MaxHealth) < 0.1f;
    }
}
